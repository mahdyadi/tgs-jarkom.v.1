import socket                   # Import socket module

portA = 60000                    # Reserve a port for your service.
s = socket.socket()             # Create a socket object
host = socket.gethostname()     # Get local machine name
s.bind((host, portA))            # Bind to the port
s.listen(5)                     # Now wait for client connection.

print ('Server listening....')

while True:
	conn, addr = s.accept()     # Establish connection with client.
	portB = 12000
	s1 = socket.socket()
	s1.bind((host,portB))
	s1.listen(1)
	conn1, addr =s1.accept()
	print ('Got connection from', addr)
	data = conn1.recv(1024).decode()
	print('Server received', repr(data))

	filename='mytext.txt'
	f = open(filename,'rb')
	l = f.read(1024)
	while (l):
		conn1.send(l)
		print('Sent ',repr(l))
		l = f.read(1024)
	f.close()

	print('Done sending')
	conn1.close()
	conn.close()