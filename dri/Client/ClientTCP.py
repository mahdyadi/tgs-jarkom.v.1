###########################
# Import required modules #
###########################

from socket import *
from os.path import getsize
from sys import getsizeof
from os.path import isfile
import re

#######################
# Constant Definition #
#######################

# RFC 959 code rep if requested file is ready
CODE_FILE_READY = 150
# RFC 959 code rep if requested file is unavailable (not found / no access)
CODE_FILE_UNAVAILABLE = 550
# RFC 959 code rep if connection/socket cannot be created
CODE_CONN_SETUP_FAILED = 425
# RFC 959 code rep if connection/socket ready to be used
CODE_CONNECTION_OPEN = 125
# RFC 959 code rep if file is failed to be written
CODE_COMMAND_ERROR = 501
# RFC 959 code rep acknowledged response
CODE_OK = 200
# RFC 959 code if requested file is busy
CODE_ERROR_WRITE = 452
# Buffer size
BUFFER_SIZE = 1024

################
# Main program #
################

# Import required modules
from socket import *
from os.path import getsize
from sys import getsizeof
from os.path import isfile
import re

# Main program part

# Enter infinite loop for connection attempts
while (True):

	# Request input: Server name / IP Address and port
	serverName = input('Server name / IP Address: ')
	serverPort = int(input('Server port: '))

	# Setup socket objects
	clientSocket = socket(AF_INET,SOCK_STREAM)
	
	# Attempt a connection
	try:
		clientSocket.connect((serverName,serverPort))
		
		# Report that connection is established and ready for request
		print('Connection established. Input your requests.')
		
		# Enter infinite loop for sending requests
		while (True):
			# Send request to server
			request = input('>>> ')
				
			# Depending on request type, server reply will differ
			reqSplit = request.split(' ',1)
			requestCmd = reqSplit[0]
			
			# Check input format and file for STOR
			if (requestCmd.lower() == 'stor'):
				try:
					while len(reqSplit) < 2:
						fname = input("Please type your filename: ")
						request = requestCmd + ' ' + fname
						print(request)
						reqSplit = request.split(' ')
						requestCmd = reqSplit[0]
					filename = reqSplit[1]
					if(not isfile(filename)):
						print("File not exists")
						requestCmd = ''
						continue
					# For the sake of (in)convenience, STOR request are sent by "STOR size <filename>" format
					request = 'stor ' + str(getsize(filename)) + ' ' + filename
				except error as err:
					print("error: {0}".format(err))
					continue
			
			#Check input for RETR
			if (requestCmd.lower() == 'retr'):
				try:
					while len(reqSplit) < 2:
						fname = input("Please type your filename: ")
						request = requestCmd + ' ' + fname
						print(request)
						reqSplit = request.split(' ',1)
						requestCmd = reqSplit[0]
				except error as err:
					print("error: {0}".format(err))
					continue
			
			# Send request to server
			# Requests which aren't stor, retr, or list won't be sent to server
			if (requestCmd.lower() == 'stor' or requestCmd.lower() == 'retr' or request.lower() == 'list' or request.lower() == 'exit'):
				print("Requesting",request.lower(),"to server")
				clientSocket.send(request.encode())
			else:
				print("Invalid request")
			
			# If request is EXIT, break from infinite loop
			# Server will also terminate thread waiting for this client
			if (request.lower() == 'exit'):
				break
			
			if (request.lower() == 'list'):
				# Request type is LIST, reply is list of files
				reply = clientSocket.recv(BUFFER_SIZE).decode()
				print(reply)				
			elif (requestCmd.lower() == 'retr'):
				# Request type is RETR
				reply = clientSocket.recv(BUFFER_SIZE).decode()
				
				# Get file name from request text
				fileName = request[5:]
				
				# Parse reply. First 'token' is file status code
				replySplit = reply.split(' ')
				fileStatus = int(replySplit[0])
				
				# Check file status code
				if (fileStatus == CODE_FILE_READY):
					# File available and ready, prepare for transfer. Report to console
					
					# Get file size and port number from reply
					fileSize = int(replySplit[1])
					serverDataPortNum = int(replySplit[2])
					
					# Report to console
					print('File available: ' + reqSplit[1] + '. Size: ' + str(fileSize) + ' Bytes. Prepare connection to port ' + str(serverDataPortNum))
					
					# Create data connection socket
					dataSocket = socket(AF_INET,SOCK_STREAM)
					dataSocket.connect((serverName,serverDataPortNum))
					
					# GET ack
					print('receive ack')
					reply = clientSocket.recv(BUFFER_SIZE).decode()
					print(reply)
					
					# SEND ack
					print('sent ack')
					clientSocket.send(str(CODE_OK).encode())
										
					# Start receiving file
					totalrecv = 0
					fileByte = bytes()
					with open(fileName,'wb') as file:
						file.write(fileByte)
					with open(fileName,'ab') as file:
						while (totalrecv < fileSize):
							fileByte = dataSocket.recv(BUFFER_SIZE)
							file.write(fileByte)
							totalrecv = totalrecv + len(fileByte)
							print("Received ",totalrecv," byte/ ",fileSize," byte")	
						
					fileByte = bytes()
					
					print('Transfer complete')
					
				elif (fileStatus == CODE_FILE_UNAVAILABLE):
					print('ERROR: File ' + fileName + ' not found')
				elif (fileStatus == CODE_ERROR_WRITE):
					print('ERROR: File ' + fileName + ' busy')
					
			elif (requestCmd.lower() == 'stor'):
			
				# Request type is STOR
				reply = clientSocket.recv(BUFFER_SIZE).decode()
				
				# Get file name from request text
				fileName = request.split(' ',2)[2]				
				
				# Parse reply. First 'token' is file status code
				replySplit = reply.split(' ')
				fileStatus = int(replySplit[0])
				
				if (fileStatus == CODE_OK):
					# File available and ready, prepare for transfer. Report to console
					
					# Get file size and port number from reply
					serverDataPortNum = int(replySplit[1])
					fileSize = int(request.split(' ')[1])
					
					# Report to console
					print('Ready to transfer: ' + reqSplit[1] + '. Size: ' + str(fileSize) + ' Bytes. Prepare connection to port ' + str(serverDataPortNum))
					
					# Create data connection socket
					print("Connecting to data server ",serverName," on port ",serverDataPortNum,"...")
					dataSocket = socket(AF_INET,SOCK_STREAM)					
					dataSocket.connect((serverName,serverDataPortNum))
					
					# GET ack
					print('receive ack')
					reply = clientSocket.recv(BUFFER_SIZE).decode()
					print(reply)
					
					fileByte = bytes()
						
					# Start sending file
					totalSent = 0
					
					with open(fileName,"rb")as file:
						while (totalSent < fileSize):
							fileByte = file.read(BUFFER_SIZE)
							sent = dataSocket.send(fileByte)
							totalSent += sent
							print("Sent ",totalSent," byte/ ",fileSize," byte")
						
					print("Sent complete")
					
					fileByte = bytes()
					# Data connection socket
					dataSocket.close()
				elif (fileStatus == CODE_FILE_UNAVAILABLE):
					print('ERROR: File ' + fileName + ' not found')
				elif (fileStatus == CODE_ERROR_WRITE):
					print('ERROR: Cannot write file',fileName)
				
	except error:
		print('Error attempting to connect to ' + serverName + ' on port ' + str(serverPort))
		input()
	
