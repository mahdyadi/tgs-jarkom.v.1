# Import required modules
from socket import *
from threading import Thread
from os import listdir
from os.path import getsize
from os.path import isfile
import re

# Define constant
MAX_CONNECTION = 20

class ServerThread(Thread):

	# RTF 959 code if data connection is open and ready for transfer
	CODE_CONNECTION_OPEN = 125
	
	# RTF 959 code if requested file is ready
	CODE_FILE_READY = 150
	
	# RTF 959 code if data connection cannot be opened
	CODE_CANNOT_OPEN_CONNETION = 425
	
	# RTF 959 code if requested file is busy
	CODE_FILE_BUSY = 450
	
	# RTF 959 code if requested file is busy
	CODE_ERROR_WRITE = 452
	
	# RTF 959 code if requested file is unavailable (not found / no access)
	CODE_FILE_UNAVAILABLE = 550
	
	# Buffer size
	BUFFER_SIZE = 1024
	
	def __init__(self,connSocket,ip,port):
		# Initialize thread
		Thread.__init__(self)
		self.connSocket = connSocket
		self.ip = ip
		self.port = port
		# Print to console
		print('Received connection request from ' + str(ip) + ' port ' + str(port))
	
	def run(self):
		exit = False
		
		while (not exit):
			# Receive input from client
			inp = self.connSocket.recv(1024).decode()
			
			# Parse input: Split between command and arguments (separated with ' ')
			inpSplit = inp.split(' ',1)
			command = inpSplit[0]
			
			# Check command type, complete each type accordingly
			if (command.lower() == 'list'):
				self.listFile()
			elif (command.lower() == 'retr'):
				self.retrieveFile(inpSplit[1])
			elif (command.lower() == 'stor'):
				inpSplit = inpSplit[1].split(' ',1)
				size = int(inpSplit[0])
				filename = inpSplit[1]
				self.storeFile(filename,size)
			elif (command.lower() == 'exit'):
				exit = True
			
		print('Connection from ' + str(self.ip) + ' terminated by client')
	
	def listFile(self):
		reply = ''
		for f in listdir('.'):
			if (not isfile(f)):
				reply = reply + f + '\n'
		for f in listdir('.'):
			if (isfile(f)):
				reply = reply + f + '\n'
		self.connSocket.send(reply.encode())
	
	def retrieveFile(self,fileName):
		if (not (fileName in listdir('.'))):
			reply = str(self.CODE_FILE_UNAVAILABLE)
			self.connSocket.send(reply.encode())
		else:
			# Report to console
			print('Client ' + str(self.ip) + ' requested file ' + fileName)
			
			# Try to open file (check if file is busy)
			try:
				file = open(fileName,'rb')
			
				# Get file size
				fileSize = getsize(fileName)
									
				# Prepare data socket
				global MAX_CONNECTION
				dataSocket = socket(AF_INET,SOCK_STREAM)
				dataSocket.bind(('',0))
				
				# Send reply to client: <ready code> <file size> <port number for data connection>
				reply = str(self.CODE_FILE_READY) + ' ' + str(fileSize) + ' ' + str(dataSocket.getsockname()[1])
				self.connSocket.send(reply.encode())
				dataSocket.listen(MAX_CONNECTION)
				
				# Listen for client data connection request
				(clientDataConnection,(clientIp,clientDataPort)) = dataSocket.accept()
				
				# Send acknowledgement that data port is open and ready for connection
				reply = str(self.CODE_CONNECTION_OPEN)
				self.connSocket.send(reply.encode())
				
				# Receive OK status from client
				okMessage = self.connSocket.recv(self.BUFFER_SIZE).decode()
				
				# Begin sending file
				totalSent = 0
				while (totalSent < fileSize):
					fileByte = file.read(self.BUFFER_SIZE)
					clientDataConnection.send(fileByte)
					totalSent += self.BUFFER_SIZE
				
				# Clear content
				fileByte = bytes()
				
				# Close file and connection
				file.close()
				clientDataConnection.close()
			
			except error:
				reply = str(self.CODE_FILE_BUSY).encode()
				self.connSocket.send(reply.encode())
	
	def storeFile(self,fileName,filesize):
		
		# Check first if file is already exist or not
		#if (not isfile(fileName)):
		#else:
			#Handle case when the filename and the file is already exist
		#	reply = str(int(452))
		#	self.connSocket.send(reply.encode())
		# Prepare data socket
		
		global MAX_CONNECTION
		dataSocket = socket(AF_INET,SOCK_STREAM)
		dataSocket.bind(('',0))
		#Send reply to client with reply = <OK code> + <Data port number>
		reply = str(int(200))+' '+ str(dataSocket.getsockname()[1])
		self.connSocket.send(reply.encode())
		dataSocket.listen(MAX_CONNECTION)
			
		# Listen for client data connection request
		(clientDataConnection,(clientIp,clientDataPort)) = dataSocket.accept()
		reply = str(self.CODE_CONNECTION_OPEN)
		self.connSocket.send(reply.encode())
		#Receive sent file from client
		totalreceived = 0 
		fileByte = bytes()
		try:
			dummy = open(fileName,'rb')
			while totalreceived < filesize:
				fileByte = fileByte + clientDataConnection.recv(filesize-totalreceived)
				totalreceived = totalreceived + len(fileByte)
				print ("Received ",totalreceived," byte as ",fileName," from ",str(clientIp))
			
			try:
			# Open file for writing
				with open(fileName,'wb') as file:
					file.write(fileByte)
			except:
				reply = str(self.CODE_ERROR_WRITE)
				self.connSocket.send(reply.encode())
			fileByte = bytes()
		except:
			reply = str(self.CODE_FILE_BUSY)
			self.connSocket.send(reply.encode())
		clientDataConnection.close()
		

# Main program part

# Request input: Port numbers
commandPort = int(input('Command Port: '))


# Setup socket objects
serverSocket = socket(AF_INET,SOCK_STREAM)
serverSocket.bind(('',commandPort))
serverSocket.listen(MAX_CONNECTION)

# Print status to console: Ready for connection requests
print('Server started. Ready for connections.')

# Enter infinite loop for receiving connections
while (True):
	(connectionSocket,(ipAddress, portNum)) = serverSocket.accept()
	
	# Create new thread for every newly accepted connection
	newConnectionThread = ServerThread(connectionSocket,ipAddress,portNum)
	newConnectionThread.start()
